module BreedsHelper
    def get_breeds_list
        begin
            JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)
        rescue Object => e
            {
              "status" => "success",
              "message" => {
                "african" => []
              }
            }
        end
    end
end
