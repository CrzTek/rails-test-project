# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
    $("select").change ->
        $(".loader").show()
        $("img").addClass("opacity-30")
        $.ajax({
            type: "POST",
            url: "/",
            data: { breed: $(".dog-list option:selected").val()},
            success: (data) ->
                $("img").attr("src", data.pic_url)
                $("h3").html(data.name)
                $("img").removeClass("opacity-30")
                $(".loader").hide()
                return false
            error: (data) ->
                return false
        })