class BreedsController < ApplicationController
  include BreedsHelper

  def index
    @breed = DogBreedFetcher.fetch
    @breeds_list = get_breeds_list["message"]
  end

  def create
    breed_type = params[:breed] || nil
    @breed = DogBreedFetcher.fetch(breed_type)
    render :json => @breed
  end
end
