class DogBreedFetcher
  attr_reader :breed_type

  def initialize(breed_type=nil)
    @breed_type  = breed_type || "affenpinscher"
    @breed = Breed.find_or_initialize_by(name: @breed_type.humanize)
  end

  def fetch
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info["message"]
    @breed.save && @breed
  end

  def self.fetch(breed_type=nil)
    breed_type ||= "affenpinscher"
    DogBreedFetcher.new(breed_type).fetch
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{ @breed_type }/images/random").body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end
end
